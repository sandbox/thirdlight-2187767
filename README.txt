Third Light Drupal Module 0.2
=============================

Overview
--------
This module provides a plugin for the CKEditor to enable the insertion of images from a Third Light IMS site.

Requirements
------------
  - Drupal 7.x,
  - PHP 5.2 or greater,
  - ckeditor 7.x-1.13 or greater

Installation / Configuration
----------------------------
Note: these instructions assume that you install the Third Light Drupal module in the
      "sites/all/modules" directory (recommended).

   1. Unzip the module files to the "sites/all/modules" directory. It should now
      contain a "thirdlight_browser" directory.
   2. Enable the module in the "Administration panel > Modules > User Interface" section.
   3. Review configuration permissions in the "Administration panel > People > Permissions" section.
   4. Enable the "Plugin for using the Third Light Browser from CKEditor" on the "Editor Appearance"
      section for profiles in the "Administration panel > Configuration > Content Authoring > CKEditor" section.
      You should then add the button to the "Used buttons" section
   5. Ensure that `<img>` is allowed in your output formats, see
      "Administration panel > Configuration > Content Authoring > Text formats"
   6. Configure the plugin for your Third Light IMS site, at
      "Administration panel > Configuration > Content Authoring > Third Light Browser > Third Light Browser Settings"
   7. Review or edit the output formats at
      "Administration panel > Configuration > Content Authoring > Third Light Browser"

Help
-------------------
If you are looking for more information, have any trouble with the configuration of the module
or if you found an issue, please review the Third Light documentation at:
  http://www.thirdlight.com/docs/display/integration/Home

