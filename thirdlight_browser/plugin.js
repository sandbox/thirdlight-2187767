// Register the plugin within the editor.
CKEDITOR.plugins.add( 'thirdlight_browser', {

    // The plugin initialization logic goes inside this method.
    init: function( editor ) {
        var config = editor.config;
        var thirdlightConfig=Drupal.settings["thirdlight_browser"];

        if (thirdlightConfig === undefined || !thirdlightConfig.enabled) {
            return;
        }

        // check that the browser has been configured
        if (!thirdlightConfig.configured) {
            editor.ui.addButton( 'thirdlight_browser',
                {
                    label : Drupal.t('Third Light Browser (disabled)'),
                    icon : this.path + 'icons/thirdlight_browser_disabled.png',
                    command : 'thirdlight_browser'
                });
            editor.addCommand( 'thirdlight_browser',
                {
                    exec : function()
                    {
                        if(thirdlightConfig.reason) {
                            alert(thirdlightConfig.reason);
                        } else {
                            alert('The Third Light Browser has not been configured yet');
                        }
                        return;
                    }
                });
            return;
        }

        // Register the toolbar buttons.
        editor.ui.addButton( 'thirdlight_browser',
            {
                label : Drupal.t('Third Light Browser'),
                icon : this.path + 'icons/thirdlight_browser.png',
                command : 'thirdlight_browser'
            });



        // command to launch the browser
        editor.addCommand( 'thirdlight_browser',
            {
                exec : function()
                {
                    if (thirdlightConfig === undefined || !thirdlightConfig.configured) {
                        alert('The Third Light Browser has not been configured yet');
                        return;
                    }
                    var app = new IMS.IframeAppOverlay(thirdlightConfig.browserUrl, {
                        top:"80px",
                        right:"20px",
                        bottom:"20px",
                        left:"20px",
                        options: thirdlightConfig.options
                    });
                    // update the image
                    app.on("cropChosen", function(cropDetails) {
                        var img = editor.document.createElement( 'img' );
                        img.setAttribute('src', cropDetails.urlDetails.url);
                        img.setAttribute('width', cropDetails.urlDetails.width);
                        img.setAttribute('height', cropDetails.urlDetails.height);

                        if(cropDetails.cropClass && thirdlightConfig.options.cropClasses) {
                            jQuery.each(thirdlightConfig.options.cropClasses, function(index, curClass) {
                                if(curClass.key == cropDetails.cropClass) {
                                    if(curClass.className) {
                                        img.setAttribute("class", curClass.className);
                                    }
                                    return false;
                                }
                            });
                        }
                        if(thirdlightConfig.titleMode && cropDetails.metadata) {
                            img.setAttribute("title", cropDetails.metadata[thirdlightConfig.titleMode] || "");
                            img.setAttribute("alt", cropDetails.metadata[thirdlightConfig.titleMode] || "");
                        }
                        editor.insertElement( img );
                    });
                }
            } );
    }
});
