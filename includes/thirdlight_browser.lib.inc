<?php
require_once("imsapiclient.php");

function thirdlight_browser_load_settings() {
    $select = db_select('thirdlight_browser_config', 'c');
    $result = $select->fields('c', array("parameter","value"))->execute();
    $arrConfig = array();
    while($row = $result->fetchAssoc())
    {
        $arrConfig[$row["parameter"]] = $row["value"];
    }
    return $arrConfig;
}

function thirdlight_browser_load_variants() {
    $select = db_select('thirdlight_browser_variants', 'v');
    $result = $select->fields('v', array("name","width","height","format","className"))->execute();
    $arrVariants = array();
    $key = 0;
    while($row = $result->fetchAssoc())
    {
        $arrVariants[] = array("key"=>$key, "name"=>$row["name"], "width"=>intval($row["width"]), "height"=>intval($row["height"]),"format"=>$row["format"],"className"=>$row["className"]);
        ++$key;
    }
    return $arrVariants;
}

function thirdlight_browser_load_themes()
{
    $options = array("light" => t("Light"), "dark" => t("Dark"), "blue" => t("Blue"));
    return $options;
}

function thirdlight_browser_load_alttextmodes()
{
    $options = array("" => t("None"), "caption" => t("Caption"), "copyright" => t("Copyright Notice"), "instructions" => t("Special Instructions"));
    return $options;
}
