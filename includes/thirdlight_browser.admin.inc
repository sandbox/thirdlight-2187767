<?php

function thirdlight_browser_admin_main() {
    module_load_include('inc', 'thirdlight_browser', 'includes/thirdlight_browser.lib');

    $output = '';

    $arrConfig = thirdlight_browser_load_settings();
    $arrVariants = thirdlight_browser_load_variants();
    $titleInfo = thirdlight_browser_load_alttextmodes();


    $siteRows = array();
    $variantRows = array();
    $siteRows[] = array(
        array('data' => t('Third Light Site')),
        array('data' => (empty($arrConfig["url"]) ? "<i>".t('Not configured')."</i>" : $arrConfig["url"]))
    );
    $siteRows[] = array(
        array('data' => t('API Key')),
        array('data' => "<i>".t(empty($arrConfig["apikey"]) ? "Not set" : "Set")."</i>")
    );
    $siteRows[] = array(
        array('data' => t('Automatic login')),
        array('data' => "<i>".t(empty($arrConfig["apikey"]) || empty($arrConfig["autologin"]) ? "Disabled" : ($arrConfig["autologin"]== "username" ? "By username":"By e-mail address"))."</i>")
    );
    if(!empty($arrConfig["theme"]))
    {
        $themeInfo = thirdlight_browser_load_themes();
        if(array_key_exists($arrConfig["theme"], $themeInfo))
        {
            $siteRows[] = array(
                array('data' => t('Theme')),
                array('data' => $themeInfo[$arrConfig["theme"]])
            );
        }
    }
    $siteRows[] = array(
        array('data' => t('Display revisions')),
        array('data' => empty($arrConfig["revisions"]) ? t('No') : t('Yes'))
    );
    $siteRows[] = array(
        array('data' => t('Display metadata')),
        array('data' => empty($arrConfig["metadata"]) ? t('No') : t('Yes'))
    );
    $siteRows[] = array(
        array('data' => t('Title for inserted images')),
        array('data' => empty($arrConfig["titlemode"]) ? "<i>".t('None')."</i>" : $titleInfo[$arrConfig["titlemode"]])
    );

    foreach($arrVariants as $thisVariant)
    {
        $variantRows[] = array(
            array('data' => $thisVariant["name"]),
            array('data' => $thisVariant["format"]." &ndash; ".$thisVariant["width"]." &#10005; ".$thisVariant["height"]),
            array('data' => l(t('edit'), '/admin/config/content/thirdlight_browser/editvariant/'.$thisVariant["name"]) . ' ' . l(t('delete'), '/admin/config/content/thirdlight_browser/deletevariant/'.$thisVariant["name"])),
        );
    }


    $output .= "<div style='padding:10px 0'><h3>".t("Global settings")."</h3>";
    $output .= theme('table', array("header" => array(t('Option'), t('Setting')), "rows" => $siteRows));
    $output .= "<div style='padding:5px'>".l(t('Change settings'), 'admin/config/content/thirdlight_browser/editsettings', array('attributes' => array('class'=>'button')));
    $output .= "</div></div><div style='padding:10px 0'>";
    $output .= "<h3>".t("Output formats")."</h3>";
    $output .= theme('table', array("header" => array(t('Name'), t('Details'), t('Operations')), "rows" => $variantRows));
    $output .= "<div style='padding:5px'>".l(t('Add output format'), 'admin/config/content/thirdlight_browser/addvariant', array('attributes' => array('class'=>'button')));
    $output .= '</div></div>';
    return $output;
}

function thirdlight_browser_admin_main_form($form, $form_state)
{
    module_load_include('inc', 'thirdlight_browser', 'includes/thirdlight_browser.lib');

    $arrSettings = thirdlight_browser_load_settings();

    $boolean_options = array("0" => t("No"), "1" => t("Yes"));
    $theme_options = thirdlight_browser_load_themes();
    $title_options = thirdlight_browser_load_alttextmodes();

    $form['url'] = array(
        '#type' => 'textfield',
        '#title' => t('Third Light Site'),
        '#default_value' => empty($arrSettings['url']) ? '' : $arrSettings['url'],
        '#required' => TRUE,
        '#description' => t('The URL of your Third Light IMS site - e.g. http://imsdemonstration.thirdlight.com/')
    );


    $form['apikey'] = array(
        '#type' => 'textfield',
        '#title' => t('Third Light API Key'),
        '#default_value' => empty($arrSettings['apikey']) ? '' : $arrSettings['apikey'],
        '#description' => t('An optional API key for your Third Light IMS site. Supplying this enables additional authentication and metadata options.')
    );

    $form['autologin'] = array(
        '#type' => 'select',
        '#title' => t('Log users in automatically'),
        '#default_value' => empty($arrSettings['autologin']) ? '0' : $arrSettings['autologin'],
        '#options' => array("0"=>"Off", "username" => "By Username", "email" => "By e-mail address"),
        '#description' => t('If a valid API key is specified, users can be logged in to the Third Light Browser automatically.')
    );

    if(!function_exists("curl_init"))
    {
        $form['apikey']['#disabled'] = true;
        $form['autologin']['#disabled'] = true;

        $form['apikey']['#default_value'] = "";
        $form['autologin']['#default_value'] = "0";

        $form['apikey']['#description'] = "<strong>".t('This requires the PHP cURL extension.')."</strong> ".$form['apikey']['#description'];

    }

    $form['revisions'] = array(
        '#type' => 'select',
        '#title' => t('Show file revisions'),
        '#default_value' => empty($arrSettings['revisions']) ? '0' : '1',
        '#options' => $boolean_options,
        '#description' => t('Third Light sites support version control. Enable this to permit access to versions other than the currently active one.')
    );

    $form['metadata'] = array(
        '#type' => 'select',
        '#title' => t('Display metadata'),
        '#default_value' => empty($arrSettings['metadata']) ? '0' : '1',
        '#options' => $boolean_options,
        '#description' => t('Third Light sites support rich metadata. Enable this to offer metadata to users of the Third Light Browser.')
    );

    $form['theme'] = array(
        '#type' => 'select',
        '#title' => t('Choose a skin'),
        '#default_value' => $arrSettings['theme'],
        '#options' => $theme_options,
    );

    $form['titlemode'] = array(
        '#type' => 'select',
        '#title' => t('Title for inserted images'),
        '#default_value' => empty($arrSettings['titlemode']) ? "" : $arrSettings["titlemode"],
        '#options' => $title_options,
        '#description' => t('This allows you to set the title and alt text of images inserted based on the metadata set in your Third Light IMS site.')
    );


    $form['actions'] = array('#tree' => FALSE, '#type' => 'actions');
    $form['actions']['submit'] = array(
        '#type' => 'submit',
        '#value' => t('Save')
    );
    $form['actions']['cancel'] = array(
        '#type' => 'link',
        '#title' => t('Cancel'),
        '#href' => 'admin/config/content/thirdlight_browser',
    );

    return $form;
}

function thirdlight_browser_admin_variant_form($form, &$form_state, $variant = null)
{
    module_load_include('inc', 'thirdlight_browser', 'includes/thirdlight_browser.lib');

    if(empty($variant)) {
        $details = array(
            "name" => "",
            "width" => "",
            "height" => "",
            "format" => "",
            "className" => "",
        );
    } else {
        $arrVariants = thirdlight_browser_load_variants();
        foreach($arrVariants as $thisVariant)
        {
            if(0 == strcasecmp($thisVariant["name"], $variant))
            {
                $details = $thisVariant;
                break;
            }
        }
        if(empty($details))
        {
            drupal_set_message(t('The output format could not be found.'), 'error');
            drupal_goto('admin/config/content/thirdlight_browser');
        }
        $form['_variant'] = array(
            "#type" => 'value',
            "#value" => $details["name"]
        );
    }

    $form['name'] = array(
        '#type' => 'textfield',
        '#title' => t('Name'),
        '#default_value' => $details["name"],
        '#required' => true
    );

    $form['width'] = array(
        '#type' => 'textfield',
        '#title' => t('Width'),
        '#default_value' => $details["width"],
        '#required' => true,
        '#description' => t('Width of the image, in pixels')
    );

    $form['height'] = array(
        '#type' => 'textfield',
        '#title' => t('Height'),
        '#default_value' => $details["height"],
        '#required' => true,
        '#description' => t('Height of the image, in pixels')
    );

    $form['format'] = array(
        '#type' => 'select',
        '#title' => t('Format'),
        '#default_value' => $details["format"],
        '#options' => array("JPG" => "JPG", "PNG" => "PNG", "GIF"=>"GIF"),
        '#description' => t('File format for the image')
    );

    $form['className'] = array(
        '#type' => 'textfield',
        '#title' => t('CSS Class'),
        '#default_value' => $details["className"],
        '#description' => t('An optional CSS class to apply to images inserted using this category.')
    );

    $form['actions'] = array('#tree' => FALSE, '#type' => 'actions');
    $form['actions']['submit'] = array(
        '#type' => 'submit',
        '#value' => t('Save')
    );
    $form['actions']['cancel'] = array(
        '#type' => 'link',
        '#title' => t('Cancel'),
        '#href' => 'admin/config/content/thirdlight_browser',
    );

    return $form;
}

function thirdlight_browser_admin_variant_deleteform($form, $form_state, $variant)
{
    module_load_include('inc', 'thirdlight_browser', 'includes/thirdlight_browser.lib');


    $arrVariants = thirdlight_browser_load_variants();
    foreach($arrVariants as $thisVariant)
    {
        if(0 == strcasecmp($thisVariant["name"], $variant))
        {
            $details = $thisVariant;
            break;
        }
    }
    if(empty($details))
    {
        drupal_set_message(t('The output format could not be found.'), 'error');
        drupal_goto('admin/config/content/thirdlight_browser');
    }
    $form['_variant'] = array(
        "#type" => 'value',
        "#value" => $details["name"]
    );

    $form['confirmmsg'] = array(
        "#type" => 'markup',
        '#markup' => "<p>".t("Are you sure you want to delete this output format? No files created using it will be affected.")."</p>"
    );

    $form['actions'] = array('#tree' => FALSE, '#type' => 'actions');
    $form['actions']['submit'] = array(
        '#type' => 'submit',
        '#value' => t('Delete')
    );
    $form['actions']['cancel'] = array(
        '#type' => 'link',
        '#title' => t('Cancel'),
        '#href' => 'admin/config/content/thirdlight_browser',
    );

    return $form;
}

function thirdlight_browser_admin_main_form_validate($form, &$form_state) {
    $data =& $form_state['values'];
    $keyValid = false;
    $arrURL = parse_url($data["url"]);
    if(empty($arrURL) || !preg_match("|^https?://|", $data["url"]))
    {
        form_set_error("url", t('The URL provided does not appear to be valid.'));
	}
    $strTestURL = $data["url"]."/apps/cmsbrowser/index.html";
	if(function_exists("curl_init"))
	{
		$rCurl = curl_init($strTestURL);
        curl_setopt($rCurl, CURLOPT_RETURNTRANSFER, true);
        $ret = curl_exec($rCurl);
		if(curl_errno($rCurl) != 0)
		{
			form_set_error("url", t('The URL provided does not be a Third Light site supporting the Third Light Browser. cURL replied: '.curl_error($rCurl)));
		}
        elseif(false === strpos($ret, "thirdlight_application"))
        {
            form_set_error("url", t('The URL provided does not be a Third Light site supporting the Third Light Browser.'));
        }
	}
    elseif(ini_get("allow_url_fopen"))
    {
		$ret = @file_get_contents($strTestURL);
		if(empty($ret) || (false === strpos($ret, "thirdlight_application")))
		{
			form_set_error("url", t('The URL provided does not be a Third Light site supporting the Third Light Browser.'));
		}
	}
    if(!empty($data["apikey"]))
    {
        try{
            new IMSApiClient($data["url"], $data["apikey"]);
            $keyValid = true;
        }
        catch(IMSApiActionException $e) {
            form_set_error("apikey", t('The API key was rejected by the Third Light IMS server.'));
        }
        catch(IMSApiPrerequisiteException $e) {
            form_set_error("apikey", t("API client prerequisite missing: ").$e->getMessage());
        }
        catch(IMSApiClientException $e) {
            form_set_error("url", t('The URL provided does not appear to be a Third Light site.'));
        }
    }
    if(!empty($data["autologin"]) && !$keyValid)
    {
        form_set_error("autologin", t('Automatic log in requires that the API key be configured correctly.'));
    }
}

function thirdlight_browser_admin_variant_deleteform_validate($form, &$form_state) {}
function thirdlight_browser_admin_variant_form_validate($form, &$form_state) {
    $data =& $form_state['values'];
    if(empty($data["_variant"]) || ($data["_variant"] != $data["name"])) {
        $arrVariants = thirdlight_browser_load_variants();
        foreach($arrVariants as $thisVariant)
        {
            if(0 == strcasecmp($thisVariant["name"], $data["name"]))
            {
                form_set_error("name", t('An output format already exists with that name'));
                break;
            }
        }
    }
    foreach(array("width", "height") as $dim) {
        $intDim = intval($data[$dim], 10);
        if($intDim != $data["$dim"]) {
            form_set_error($dim, t(ucfirst($dim).' must be specified as a number in pixels.'));
        }
        elseif($intDim < 1) {
            form_set_error($dim, t(ucfirst($dim).' must be a positive number of pixels.'));
        }
        elseif($intDim > 10000) {
            form_set_error($dim, t(ucfirst($dim).' exceeds supported limit of 10,000px.'));
        }
    }
}

function thirdlight_browser_admin_main_form_submit($form, &$form_state) {
    module_load_include('inc', 'thirdlight_browser', 'includes/thirdlight_browser.lib');

    $data =& $form_state['values'];

    db_merge('thirdlight_browser_config')->key(array('parameter'=>'url'))->fields(array("value"=>$data["url"]))->execute();
    db_merge('thirdlight_browser_config')->key(array('parameter'=>'theme'))->fields(array("value"=>$data["theme"]))->execute();
    db_merge('thirdlight_browser_config')->key(array('parameter'=>'revisions'))->fields(array("value"=>$data["revisions"]))->execute();
    db_merge('thirdlight_browser_config')->key(array('parameter'=>'metadata'))->fields(array("value"=>$data["metadata"]))->execute();
    db_merge('thirdlight_browser_config')->key(array('parameter'=>'titlemode'))->fields(array("value"=>$data["titlemode"]))->execute();
    db_merge('thirdlight_browser_config')->key(array('parameter'=>'apikey'))->fields(array("value"=>$data["apikey"]))->execute();
    db_merge('thirdlight_browser_config')->key(array('parameter'=>'autologin'))->fields(array("value"=>$data["autologin"]))->execute();


    drupal_set_message(t('The Third Light Browser settings were saved.'));
    $form_state['redirect'] = 'admin/config/content/thirdlight_browser';
}
function thirdlight_browser_admin_variant_form_submit($form, &$form_state) {
    module_load_include('inc', 'thirdlight_browser', 'includes/thirdlight_browser.lib');

    $data =& $form_state['values'];
    if(!empty($data["_variant"]))
    {
        db_delete('thirdlight_browser_variants')
            ->condition('name', $data["_variant"])
            ->execute();
    }

    db_insert('thirdlight_browser_variants')
        ->fields(array(
            "name" => $data['name'],
            "width" => $data['width'],
            "height" => $data['height'],
            "format" => $data['format'],
            "className" => $data['className'],
        ))
        ->execute();

    drupal_set_message(t('The output format settings were saved.'));
    $form_state['redirect'] = 'admin/config/content/thirdlight_browser';
}

function thirdlight_browser_admin_variant_deleteform_submit($form, &$form_state) {
    module_load_include('inc', 'thirdlight_browser', 'includes/thirdlight_browser.lib');

    $data =& $form_state['values'];
    if(!empty($data["_variant"]))
    {
        db_delete('thirdlight_browser_variants')
            ->condition('name', $data["_variant"])
            ->execute();
    }

    drupal_set_message(t('The output format was deleted.'));
    $form_state['redirect'] = 'admin/config/content/thirdlight_browser';
}
